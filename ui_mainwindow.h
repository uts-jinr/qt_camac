/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QPushButton *SEND_TO_USB_pushButton;
    QPushButton *Sel_Bit_12;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QSpinBox *N_spinBox;
    QSpinBox *A_spinBox;
    QSpinBox *F_spinBox;
    QPushButton *Sel_Bit_11;
    QPushButton *Sel_Bit_10;
    QPushButton *Sel_Bit_09;
    QPushButton *Sel_Bit_08;
    QPushButton *Sel_Bit_07;
    QPushButton *Sel_Bit_06;
    QPushButton *Sel_Bit_05;
    QPushButton *Sel_Bit_04;
    QPushButton *Sel_Bit_03;
    QPushButton *Sel_Bit_02;
    QPushButton *Sel_Bit_01;
    QPushButton *Sel_Bit_13;
    QPushButton *Sel_Bit_14;
    QPushButton *Sel_Bit_16;
    QPushButton *Sel_Bit_15;
    QLabel *NAF_Data_Label;
    QLabel *label_4;
    QPushButton *Clear_All;
    QLabel *X_label;
    QLabel *X_label_2;
    QLabel *label_8;
    QLabel *Status_label;
    QPushButton *GET_pushButton;
    QPushButton *GET_Stop_pushButton;
    QPushButton *pushButton_stop_Sending;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1135, 684);
        QFont font;
        font.setPointSize(13);
        font.setBold(true);
        font.setWeight(75);
        MainWindow->setFont(font);
        MainWindow->setAnimated(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(110, 20, 31, 31));
        QFont font1;
        font1.setPointSize(14);
        label->setFont(font1);
        label->setTextFormat(Qt::RichText);
        label->setWordWrap(true);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(300, 30, 31, 21));
        label_2->setFont(font1);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(520, 30, 21, 21));
        label_3->setFont(font1);
        SEND_TO_USB_pushButton = new QPushButton(centralWidget);
        SEND_TO_USB_pushButton->setObjectName(QStringLiteral("SEND_TO_USB_pushButton"));
        SEND_TO_USB_pushButton->setGeometry(QRect(670, 72, 171, 41));
        QFont font2;
        font2.setPointSize(12);
        SEND_TO_USB_pushButton->setFont(font2);
        Sel_Bit_12 = new QPushButton(centralWidget);
        Sel_Bit_12->setObjectName(QStringLiteral("Sel_Bit_12"));
        Sel_Bit_12->setGeometry(QRect(260, 190, 51, 31));
        Sel_Bit_12->setFont(font2);
        Sel_Bit_12->setAutoFillBackground(false);
        Sel_Bit_12->setStyleSheet(QStringLiteral(""));
        Sel_Bit_12->setCheckable(true);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(100, 50, 61, 21));
        label_5->setFont(font1);
        label_5->setTextFormat(Qt::RichText);
        label_5->setWordWrap(true);
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(290, 50, 61, 21));
        label_6->setFont(font1);
        label_6->setTextFormat(Qt::RichText);
        label_6->setWordWrap(true);
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(500, 50, 61, 20));
        label_7->setFont(font1);
        label_7->setTextFormat(Qt::RichText);
        label_7->setWordWrap(true);
        N_spinBox = new QSpinBox(centralWidget);
        N_spinBox->setObjectName(QStringLiteral("N_spinBox"));
        N_spinBox->setGeometry(QRect(50, 80, 151, 51));
        QFont font3;
        font3.setPointSize(22);
        font3.setBold(false);
        font3.setWeight(50);
        N_spinBox->setFont(font3);
        N_spinBox->setButtonSymbols(QAbstractSpinBox::PlusMinus);
        N_spinBox->setValue(2);
        A_spinBox = new QSpinBox(centralWidget);
        A_spinBox->setObjectName(QStringLiteral("A_spinBox"));
        A_spinBox->setGeometry(QRect(250, 80, 151, 51));
        A_spinBox->setFont(font3);
        F_spinBox = new QSpinBox(centralWidget);
        F_spinBox->setObjectName(QStringLiteral("F_spinBox"));
        F_spinBox->setGeometry(QRect(450, 80, 151, 51));
        F_spinBox->setFont(font3);
        Sel_Bit_11 = new QPushButton(centralWidget);
        Sel_Bit_11->setObjectName(QStringLiteral("Sel_Bit_11"));
        Sel_Bit_11->setGeometry(QRect(320, 190, 51, 31));
        Sel_Bit_11->setFont(font2);
        Sel_Bit_11->setAutoFillBackground(false);
        Sel_Bit_11->setStyleSheet(QStringLiteral(""));
        Sel_Bit_11->setCheckable(true);
        Sel_Bit_10 = new QPushButton(centralWidget);
        Sel_Bit_10->setObjectName(QStringLiteral("Sel_Bit_10"));
        Sel_Bit_10->setGeometry(QRect(380, 190, 51, 31));
        Sel_Bit_10->setFont(font2);
        Sel_Bit_10->setAutoFillBackground(false);
        Sel_Bit_10->setStyleSheet(QStringLiteral(""));
        Sel_Bit_10->setCheckable(true);
        Sel_Bit_09 = new QPushButton(centralWidget);
        Sel_Bit_09->setObjectName(QStringLiteral("Sel_Bit_09"));
        Sel_Bit_09->setGeometry(QRect(440, 190, 51, 31));
        Sel_Bit_09->setFont(font2);
        Sel_Bit_09->setAutoFillBackground(false);
        Sel_Bit_09->setStyleSheet(QStringLiteral(""));
        Sel_Bit_09->setCheckable(true);
        Sel_Bit_08 = new QPushButton(centralWidget);
        Sel_Bit_08->setObjectName(QStringLiteral("Sel_Bit_08"));
        Sel_Bit_08->setGeometry(QRect(500, 190, 51, 31));
        Sel_Bit_08->setFont(font2);
        Sel_Bit_08->setAutoFillBackground(false);
        Sel_Bit_08->setStyleSheet(QStringLiteral(""));
        Sel_Bit_08->setCheckable(true);
        Sel_Bit_07 = new QPushButton(centralWidget);
        Sel_Bit_07->setObjectName(QStringLiteral("Sel_Bit_07"));
        Sel_Bit_07->setGeometry(QRect(560, 190, 51, 31));
        Sel_Bit_07->setFont(font2);
        Sel_Bit_07->setAutoFillBackground(false);
        Sel_Bit_07->setStyleSheet(QStringLiteral(""));
        Sel_Bit_07->setCheckable(true);
        Sel_Bit_06 = new QPushButton(centralWidget);
        Sel_Bit_06->setObjectName(QStringLiteral("Sel_Bit_06"));
        Sel_Bit_06->setGeometry(QRect(620, 190, 51, 31));
        Sel_Bit_06->setFont(font2);
        Sel_Bit_06->setAutoFillBackground(false);
        Sel_Bit_06->setStyleSheet(QStringLiteral(""));
        Sel_Bit_06->setCheckable(true);
        Sel_Bit_05 = new QPushButton(centralWidget);
        Sel_Bit_05->setObjectName(QStringLiteral("Sel_Bit_05"));
        Sel_Bit_05->setGeometry(QRect(680, 190, 51, 31));
        Sel_Bit_05->setFont(font2);
        Sel_Bit_05->setAutoFillBackground(false);
        Sel_Bit_05->setStyleSheet(QStringLiteral(""));
        Sel_Bit_05->setCheckable(true);
        Sel_Bit_04 = new QPushButton(centralWidget);
        Sel_Bit_04->setObjectName(QStringLiteral("Sel_Bit_04"));
        Sel_Bit_04->setGeometry(QRect(740, 190, 51, 31));
        Sel_Bit_04->setFont(font2);
        Sel_Bit_04->setAutoFillBackground(false);
        Sel_Bit_04->setStyleSheet(QStringLiteral(""));
        Sel_Bit_04->setCheckable(true);
        Sel_Bit_03 = new QPushButton(centralWidget);
        Sel_Bit_03->setObjectName(QStringLiteral("Sel_Bit_03"));
        Sel_Bit_03->setGeometry(QRect(800, 190, 51, 31));
        Sel_Bit_03->setFont(font2);
        Sel_Bit_03->setAutoFillBackground(false);
        Sel_Bit_03->setStyleSheet(QStringLiteral(""));
        Sel_Bit_03->setCheckable(true);
        Sel_Bit_02 = new QPushButton(centralWidget);
        Sel_Bit_02->setObjectName(QStringLiteral("Sel_Bit_02"));
        Sel_Bit_02->setGeometry(QRect(860, 190, 51, 31));
        Sel_Bit_02->setFont(font2);
        Sel_Bit_02->setAutoFillBackground(false);
        Sel_Bit_02->setStyleSheet(QStringLiteral(""));
        Sel_Bit_02->setCheckable(true);
        Sel_Bit_01 = new QPushButton(centralWidget);
        Sel_Bit_01->setObjectName(QStringLiteral("Sel_Bit_01"));
        Sel_Bit_01->setGeometry(QRect(920, 190, 51, 31));
        QFont font4;
        font4.setPointSize(12);
        font4.setKerning(false);
        Sel_Bit_01->setFont(font4);
        Sel_Bit_01->setAutoFillBackground(false);
        Sel_Bit_01->setStyleSheet(QStringLiteral(""));
        Sel_Bit_01->setCheckable(true);
        Sel_Bit_01->setChecked(false);
        Sel_Bit_01->setFlat(false);
        Sel_Bit_13 = new QPushButton(centralWidget);
        Sel_Bit_13->setObjectName(QStringLiteral("Sel_Bit_13"));
        Sel_Bit_13->setGeometry(QRect(200, 190, 51, 31));
        Sel_Bit_13->setFont(font2);
        Sel_Bit_13->setAutoFillBackground(false);
        Sel_Bit_13->setStyleSheet(QStringLiteral(""));
        Sel_Bit_13->setCheckable(true);
        Sel_Bit_14 = new QPushButton(centralWidget);
        Sel_Bit_14->setObjectName(QStringLiteral("Sel_Bit_14"));
        Sel_Bit_14->setGeometry(QRect(140, 190, 51, 31));
        Sel_Bit_14->setFont(font2);
        Sel_Bit_14->setAutoFillBackground(false);
        Sel_Bit_14->setStyleSheet(QStringLiteral(""));
        Sel_Bit_14->setCheckable(true);
        Sel_Bit_16 = new QPushButton(centralWidget);
        Sel_Bit_16->setObjectName(QStringLiteral("Sel_Bit_16"));
        Sel_Bit_16->setGeometry(QRect(20, 190, 51, 31));
        Sel_Bit_16->setFont(font2);
        Sel_Bit_16->setAutoFillBackground(false);
        Sel_Bit_16->setStyleSheet(QStringLiteral(""));
        Sel_Bit_16->setCheckable(true);
        Sel_Bit_15 = new QPushButton(centralWidget);
        Sel_Bit_15->setObjectName(QStringLiteral("Sel_Bit_15"));
        Sel_Bit_15->setGeometry(QRect(80, 190, 51, 31));
        Sel_Bit_15->setFont(font2);
        Sel_Bit_15->setAutoFillBackground(false);
        Sel_Bit_15->setStyleSheet(QStringLiteral(""));
        Sel_Bit_15->setCheckable(true);
        NAF_Data_Label = new QLabel(centralWidget);
        NAF_Data_Label->setObjectName(QStringLiteral("NAF_Data_Label"));
        NAF_Data_Label->setGeometry(QRect(220, 310, 241, 21));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(210, 330, 261, 20));
        QFont font5;
        font5.setPointSize(10);
        font5.setBold(false);
        font5.setWeight(50);
        label_4->setFont(font5);
        Clear_All = new QPushButton(centralWidget);
        Clear_All->setObjectName(QStringLiteral("Clear_All"));
        Clear_All->setGeometry(QRect(680, 250, 151, 61));
        X_label = new QLabel(centralWidget);
        X_label->setObjectName(QStringLiteral("X_label"));
        X_label->setGeometry(QRect(45, 540, 16, 21));
        X_label_2 = new QLabel(centralWidget);
        X_label_2->setObjectName(QStringLiteral("X_label_2"));
        X_label_2->setGeometry(QRect(123, 540, 16, 21));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(70, 420, 681, 16));
        Status_label = new QLabel(centralWidget);
        Status_label->setObjectName(QStringLiteral("Status_label"));
        Status_label->setGeometry(QRect(780, 20, 121, 16));
        GET_pushButton = new QPushButton(centralWidget);
        GET_pushButton->setObjectName(QStringLiteral("GET_pushButton"));
        GET_pushButton->setGeometry(QRect(870, 250, 201, 61));
        GET_Stop_pushButton = new QPushButton(centralWidget);
        GET_Stop_pushButton->setObjectName(QStringLiteral("GET_Stop_pushButton"));
        GET_Stop_pushButton->setGeometry(QRect(930, 340, 141, 51));
        GET_Stop_pushButton->setFlat(false);
        pushButton_stop_Sending = new QPushButton(centralWidget);
        pushButton_stop_Sending->setObjectName(QStringLiteral("pushButton_stop_Sending"));
        pushButton_stop_Sending->setGeometry(QRect(930, 70, 141, 51));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1135, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label->setText(QApplication::translate("MainWindow", "N", 0));
        label_2->setText(QApplication::translate("MainWindow", " A", 0));
        label_3->setText(QApplication::translate("MainWindow", "F", 0));
        SEND_TO_USB_pushButton->setText(QApplication::translate("MainWindow", "\320\227\320\220\320\224\320\220\320\242\320\254", 0));
        Sel_Bit_12->setText(QApplication::translate("MainWindow", "12", 0));
        label_5->setText(QApplication::translate("MainWindow", "2 - 23", 0));
        label_6->setText(QApplication::translate("MainWindow", "0 - 15", 0));
        label_7->setText(QApplication::translate("MainWindow", "0 - 31", 0));
#ifndef QT_NO_TOOLTIP
        N_spinBox->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>values 1-23</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        Sel_Bit_11->setText(QApplication::translate("MainWindow", "11", 0));
        Sel_Bit_10->setText(QApplication::translate("MainWindow", "10", 0));
        Sel_Bit_09->setText(QApplication::translate("MainWindow", "09", 0));
        Sel_Bit_08->setText(QApplication::translate("MainWindow", "08", 0));
        Sel_Bit_07->setText(QApplication::translate("MainWindow", "07", 0));
        Sel_Bit_06->setText(QApplication::translate("MainWindow", "06", 0));
        Sel_Bit_05->setText(QApplication::translate("MainWindow", "05", 0));
        Sel_Bit_04->setText(QApplication::translate("MainWindow", "04", 0));
        Sel_Bit_03->setText(QApplication::translate("MainWindow", "03", 0));
        Sel_Bit_02->setText(QApplication::translate("MainWindow", "02", 0));
        Sel_Bit_01->setText(QApplication::translate("MainWindow", "01", 0));
        Sel_Bit_13->setText(QApplication::translate("MainWindow", "13", 0));
        Sel_Bit_14->setText(QApplication::translate("MainWindow", "14", 0));
        Sel_Bit_16->setText(QApplication::translate("MainWindow", "16", 0));
        Sel_Bit_15->setText(QApplication::translate("MainWindow", "15", 0));
        NAF_Data_Label->setText(QApplication::translate("MainWindow", "00       00       00         00", 0));
        label_4->setText(QApplication::translate("MainWindow", " 7..0        15 - 8     23 - 16       31- 24", 0));
        Clear_All->setText(QApplication::translate("MainWindow", "\320\241\320\221\320\240\320\236\320\241\320\230\320\242\320\254", 0));
        X_label->setText(QApplication::translate("MainWindow", "X", 0));
        X_label_2->setText(QApplication::translate("MainWindow", "Q", 0));
        label_8->setText(QApplication::translate("MainWindow", "16                                      12                                        8                                         4", 0));
        Status_label->setText(QApplication::translate("MainWindow", "Default", 0));
        GET_pushButton->setText(QApplication::translate("MainWindow", "Get_From_Dev", 0));
        GET_Stop_pushButton->setText(QApplication::translate("MainWindow", "STOP Getting", 0));
        pushButton_stop_Sending->setText(QApplication::translate("MainWindow", "Stop Sending", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
