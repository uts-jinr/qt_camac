
#include "mainwindow.h"
//#include "mythread.h"
#include <iostream>

#include <boost/chrono/include.hpp>

#include <boost/chrono/chrono_io.hpp>
#include <boost/chrono/system_clocks.hpp>
#include <boost/chrono/io/timezone.hpp>
#include <iomanip>

using namespace std;
using namespace boost::chrono;


//MyThread::MyThread(QObject *parent) : QObject(parent)
MyThread::MyThread()
{
 finthread = false;
}

MyThread::~MyThread()
{

}
//+++++++++++++
void MyThread::A_SLOT() // ОБработчик для thread_A прием данных в ПК от железа по каналу А  QThread::usleep(150);
{

    while (!this->finthread)
    {
        QThread::usleep(10);
/*        boost::chrono::steady_clock::time_point start= boost::chrono::steady_clock::now();
        boost::chrono::steady_clock::duration delay= boost::chrono::microseconds(50);
        while (boost::chrono::steady_clock::now() - start <= delay) {;}
*/

  //Вызываем функцию или связываем с функциями marsohod.cpp через сигнал/слот
    emit Process_A(); // Запись данных из железа  в ПК по каналу А
    }
    emit A_finished();


//emit finished();

}
//+++++++++++=

void MyThread::B_SLOT()       // ОБработчик для thread_2
{
    while (!this->finthread)
    {
//        QThread::usleep(10);

        boost::chrono::steady_clock::time_point start= boost::chrono::steady_clock::now();
        boost::chrono::steady_clock::duration delay= boost::chrono::microseconds(50);
        while (boost::chrono::steady_clock::now() - start <= delay) {;}


  //Вызываем функцию или связываем с функциями marsohod.cpp через сигнал/слот
    emit Process_B(); // Запись данных из ПК в железо по каналу B
    }
    emit B_finished();
}

