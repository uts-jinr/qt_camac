#-------------------------------------------------
#
# Project created by QtCreator 2017-01-25T09:28:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KK_Thread_USB_01
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    marsohod.cpp \
    mythread.cpp

HEADERS  += mainwindow.h \
    evkuz.h \
    mythread.h

FORMS    += mainwindow.ui


#Тут лежит файл ftd2xx.h
INCLUDEPATH += $$PWD/../../../FPGA/MAX2_DevKit/Drivers_x64_v2.12.16/


#Выбрал i386 (32-разрядный вариант) т.к. компилятор 32x
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../FPGA/MAX2_DevKit/Drivers_x64_v2.12.16/i386/ -lftd2xx
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../FPGA/MAX2_DevKit/Drivers_x64_v2.12.16/i386/ -lftd2xx

INCLUDEPATH += $$PWD/../../../FPGA/MAX2_DevKit/Drivers_x64_v2.12.16/i386
DEPENDPATH += $$PWD/../../../FPGA/MAX2_DevKit/Drivers_x64_v2.12.16/i386


INCLUDEPATH += d:\Qt\boost_1_61_0\
DEPENDPATH += d:\Qt\boost_1_61_0\
INCLUDEPATH += $$PWD/../../../../Qt/boost_chrono/boost/bin.v2/libs/chrono/build/gcc-mingw-4.8.2/release
DEPENDPATH += $$PWD/../../../../Qt/boost_chrono/boost/bin.v2/libs/chrono/build/gcc-mingw-4.8.2/release



LIBS += d:\Qt\boost_chrono\boost\bin.v2\libs\system\build\gcc-mingw-4.8.2\debug\libboost_system-mgw48-d-1_61.dll
LIBS += d:\Qt\boost_chrono\boost\bin.v2\libs\chrono\build\gcc-mingw-4.8.2\release\libboost_chrono-mgw48-1_61.dll


# release - имя библиотеки без буквы 'd' в конце !!!!!!!!!!!!!!!!
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Qt/boost_chrono/boost/bin.v2/libs/chrono/build/gcc-mingw-4.8.2/release/ -llibboost_chrono-mgw48-1_61
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Qt/boost_chrono/boost/bin.v2/libs/chrono/build/gcc-mingw-4.8.2/debug/ -llibboost_chrono-mgw48-d-1_61

