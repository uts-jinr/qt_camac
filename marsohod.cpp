#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <stdlib.h>

#define OneSector 64*1024
#define SectorNum 2000

FT_HANDLE ftHandle; // Handle of the FTDI device
FT_STATUS ftStatus; // Result of each D2XX call
FT_HANDLE ftHandle_A;
FT_HANDLE ftHandle_B;

DWORD dwNumDevs; // The number of devices
DWORD dwNumBytesToRead = 0; // Number of bytes available to read in the driver's input buffer
DWORD dwNumBytesRead;
DWORD dwNumBytesSent;
DWORD dwNumBytesToSend;
//unsigned char byOutputBuffer[1024]; // Buffer to hold MPSSE commands and data to be sent to the FT2232H
int ft2232H ; // High speed device (FTx232H) found. Default - full speed, i.e. FT2232D
DWORD dwClockDivisor = 0;
DWORD dwCount;
char SerialNumber[16+1];
char Description[64+1];

//char byOutputBuffer[64];
//char byInputBuffer[64]; // Buffer to hold data readed from the FT2232H

//char RxBuffer[OneSector];

DWORD Flags;
DWORD ID;
DWORD Type;
DWORD LocId;
UCHAR Mask = 0xff;
UCHAR Mode;
UCHAR LatencyTimer = 2; //16ms default

QString str;

int DevNumber; //Номер НАШЕГО девайса

int DevNumber_A; //Порт А, Номер НАШЕГО девайса
int DevNumber_B; //Порт B, Номер НАШЕГО девайса


//int DevNumber; //Номер НАШЕГО девайса

//+++++++++++++++++++++ EK +++++++++++++++++++++++++++++++++++
FT_DEVICE_LIST_INFO_NODE *devInfo;

const char Morph_ID[] = "FT112244EKA"; //Этот девайс будем выбирать среди всех доступных
const char Mini_ID_Sem[] = "FT112244SemA";

const char Mini_ID_A[] = "Camac_021216A"; //FT112244EKA
const char Mini_ID_B[] = "Camac_021216B"; //FT112244EKB

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void MainWindow::ftdi_init(void)
{
    FT_DEVICE ftDevice;
    DWORD deviceID;
    QString str;

    ftStatus = FT_CreateDeviceInfoList(&dwNumDevs);

    // Get the number of FTDI devices
    if (ftStatus != FT_OK) // Did the command execute OK?
    {
        FTDI_Connected = FALSE; update();ui->Status_label->setText("Связи нет Create");
       // return ; // Exit and should check connection periodically
                   //return не работает !!! Нельзя его использовать
    }

    if (dwNumDevs < 1) // Exit if we don't see any
    {
        FTDI_Connected = FALSE; update();ui->Status_label->setText("Связи нет dwNumDevs");
      //  return ; // Exist with error
                   //return не работает !!! Нельзя его использовать
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FT_GetDeviceInfoList +++++++++++++++++++++++++++++++++
    FT_HANDLE ftHandleTemp;
    DWORD Flags;
    DWORD ID;
    DWORD Type;
    DWORD LocId;
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ FT_GetDeviceInfoDetail +++++++++++++++++++++++++++++++++
    // Эта функция дает читаемые строки у SerialNumber и Description
    for (int i = 0; i < dwNumDevs; i++)
    {
     // Перебираем в цикле и по каждому устройству вызываем детали
     // Нам нужен только указатель ftHandleTemp и строка с номером SerialNumber,  остальные данные тут не нужны.
        ftStatus = FT_GetDeviceInfoDetail(i, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandleTemp);

        if (ftStatus == FT_OK)
        {
            ft2232H = qstrcmp(SerialNumber, Mini_ID_A); //
            if (ft2232H == 0) {
                DevNumber_A = i; FTDI_Connected = TRUE; update();// ui->Status_label->setText("Подключено");

            }

            ft2232H = qstrcmp(SerialNumber, Mini_ID_B); //
            if (ft2232H == 0) {
                DevNumber_B = i; FTDI_Connected = TRUE; update(); //ui->Status_label->setText("Подключено");

            }
            //++++++++++++++++++++++++++++++++++++++++++++++

        }//if
    } //for

    //str.sprintf(" Camac Device has number %d\n",DevNumber);ui->FTDI_List_textBrowser->append(str);

    //Тут опрашиваем конкретно порты А и В, т.к. уже известны их номера.
    ftStatus = FT_GetDeviceInfoDetail(DevNumber_A, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandle_A);
    if (ftStatus == FT_OK)
    {
      if (FTDI_Connected & A_Open ) ui->Status_label->setText("подключено"); //USB подключено, порты открыты
      else {
            ftStatus = FT_Open(DevNumber_A, &ftHandle_A);
            if (ftStatus == FT_OK)
             {
                FTDI_Connected = TRUE; update(); A_Open = TRUE; ui->Status_label->setText("подключено");
             }
                ftStatus |= FT_SetUSBParameters(ftHandle_A,0x00040,0x00040);
           //  else {FTDI_Connected = FALSE; update(); A_Open = FALSE; ui->Status_label->setText("нет связи"); }
      }
    }
    else {FTDI_Connected = FALSE; update(); A_Open = FALSE; FT_Close(&ftHandle_A); ui->Status_label->setText("нет связи A"); }//(ftStatus == FT_OK)
//++++++++++++++++++++++++
/*
    ftStatus = FT_GetDeviceInfoDetail(DevNumber_B, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandle_B);
    if (ftStatus == FT_OK)
    {
      if (FTDI_Connected & B_Open ) ui->Status_label->setText("подключено"); //USB подключено, порты открыты
      else {
            ftStatus = FT_Open(DevNumber_B, &ftHandle_B);
            if (ftStatus == FT_OK)
             {
                FTDI_Connected = TRUE; update(); B_Open = TRUE; ui->Status_label->setText("подключено");
             }
             //else {FTDI_Connected = FALSE; update(); B_Open = FALSE; ui->Status_label->setText("нет связи"); }
      }
    }

    else {FTDI_Connected = FALSE; update(); B_Open = FALSE; FT_Close(&ftHandle_B);ui->Status_label->setText("нет связи B"); }//(ftStatus == FT_OK)

*/
    filename = "../Camac_Thread_data.bin";
    mfile.setFileName(filename);


/*
    if (!mfile.open(QIODevice::ReadWrite | QIODevice::Truncate ))
    {

       // ui->File_Open_label->setText("File does not exist");
        str.sprintf("File does not exist");ui->FTDI_List_textBrowser->append(str);

        return;
    }

    else
    {
        // ui->File_Open_label->setText("File is opened");
         str.sprintf("File is opened");ui->FTDI_List_textBrowser->append(str);
    }

*/

}
//++++++++++++++++++++++++++++++++++++++++++++++
//Запускаем поток на получение данных из Железки в ПК. Канал А.

void MainWindow::on_GET_pushButton_clicked()
{

    ftStatus = FT_GetDeviceInfoDetail(DevNumber_A, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandle_A);
    ftStatus = FT_Open(DevNumber_A, &ftHandle_A); ftStatus |= FT_SetUSBParameters(ftHandle_A,0x00040,0x00040);
 //   str.sprintf(" FTHANDLE A %d : 0x%x\n", DevNumber_A, ftHandle_A);ui->FTDI_List_textBrowser->append(str);


  chan_A->finthread = FALSE;
  thread_A->start(QThread::HighPriority); //Считываем в ПК из Железа по каналу А

}
//++++++++++++++++++++++++++++++++++++++++++++++++

//Запускаем поток на отправку из ПК в Железо. Канал В
/*
void MainWindow::on_SEND_pushButton_clicked()
{
  ftStatus = FT_GetDeviceInfoDetail(DevNumber_B, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandle_B);
  ftStatus = FT_Open(DevNumber_B, &ftHandle_B);
 // str.sprintf(" FTHANDLE B %d : 0x%x\n", DevNumber_B, ftHandle_B);ui->FTDI_List_textBrowser->append(str);

  chan_B->finthread = FALSE;
  thread_B->start(QThread::HighPriority); // TimeCriticalPriority HighestPriority Запускаем поток, и у нас генерируется сигнал started()====> Срабатывает слот B_SLOT(),
                                            // в котором и происходит уже отправка данных USB. !
}
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++


//Слот обработки сигнала Process_B - запись данных в канал B
//void MainWindow::Write_B()
//void MainWindow::on_SEND_pushButton_clicked()
void MainWindow::on_SEND_TO_USB_pushButton_clicked()
{
  //  timer->stop();
    ftStatus = FT_GetDeviceInfoDetail(DevNumber_B, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandle_B);
    ftStatus = FT_Open(DevNumber_B, &ftHandle_B); ftStatus |= FT_SetUSBParameters(ftHandle_B,0x00040,0x00040);
   // str.sprintf(" FTHANDLE B %d : 0x%x\n", DevNumber_B, ftHandle_B);ui->FTDI_List_textBrowser->append(str);

    chan_B->finthread = FALSE;
    thread_B->start(QThread::HighPriority); // TimeCriticalPriority HighestPriority Запускаем поток, и у нас генерируется сигнал started()====> Срабатывает слот B_SLOT(),



}
void MainWindow::Write_B()
{

    // Теперь NAF_Bits и ToDev_Bits объединяем в 1 32 битное слово

    ftStatus = FT_Write(ftHandle_B, byOutputBuffer,TRANSFER_SIZE, &dwNumBytesSent); //byOutputBuffer
    if (ftStatus == FT_OK) {  FTDI_Connected = TRUE; }
    else
    {
     ui->Status_label->setText("error !!!");
     FTDI_Connected = FALSE;
     B_Open = FALSE;
     FT_Close(ftHandle_B);
     // И поток тоже останавливаем.
      chan_B->finthread = TRUE;

    }

    update();
 //   ftStatus = FT_Purge(ftHandle_A, FT_PURGE_RX);
 //   ftStatus = FT_Purge(ftHandle_A, FT_PURGE_TX);

  //  ftStatus = FT_Purge(ftHandle_B, FT_PURGE_RX);
  //  ftStatus = FT_Purge(ftHandle_B, FT_PURGE_TX);

}
//===================================================
//Теперь считывам данные
// 3-й аргумент - это сколько ожидаем, 4-й аргумент - сколько на самом деле пришло
void MainWindow::Read_A()
{
ftStatus = FT_Read(ftHandle_A, byInputBuffer,64,&dwNumBytesRead); //RECEIVE_SIZE
if (ftStatus == FT_OK) {  FTDI_Connected = TRUE; }
else
{
    timer->stop();
    FTDI_Connected = FALSE;//Флаг выставили
    A_Open = FALSE;
    chan_A->finthread = TRUE;
    //if (!chan_A->finthread)
        FT_Close(ftHandle_A);
    // И поток тоже останавливаем.

   //  chan_A->A_finished();
     Purge_IO_Buffers();
     update();
     return;
}
update(); //Перерисовываем
/*
// И вот теперь пишем в файл.
if (mfile.isOpen())
{

    mfile.write(byInputBuffer, RECEIVE_SIZE); //databuf.constData(), qstrlen(databuf) dwNumBytesRead byInputBuffer mystr
}
else {ui->FTDI_List_textBrowser->append("Нет данных для записи\n");}
*/
ftStatus = FT_Purge(ftHandle_A, FT_PURGE_RX);
//ftStatus = FT_Purge(ftHandle_A, FT_PURGE_TX);
}
//++++++++++++ Проверяем состояние подключения FTDI-устройств
void MainWindow::onTimer()
{
    ftdi_init();

}
//===================================================
// Stop sending data, Channel B
void MainWindow::on_pushButton_stop_Sending_clicked()
{
    FT_Close(ftHandle_B);
    chan_B->finthread = TRUE;

}

//+++++++++++++++++++
// Stop sending data, Channel A
void MainWindow::on_GET_Stop_pushButton_clicked()
{
    FT_Close(ftHandle_A);
    chan_A->finthread = TRUE;
}




//+++++++++++++++++++
void MainWindow::on_pushButton_Read_data_clicked()
{
    DWORD EventDWord;
    DWORD TxBytes;
    DWORD RxBytes;
    DWORD BytesReceived;
    char RxBuffer[64];
    DWORD dwSum = 0;
    FT_HANDLE fthndl;
    UINT16  rdData, numBytes;
    char udata[10];


            ftStatus = FT_Read(ftHandle_A,RxBuffer,5,&BytesReceived);
            if (ftStatus == FT_OK) {
    // FT_Read OK
/*        str.sprintf(" Для девайса %d, %s\n",DevNumber, Description);ui->FTDI_List_textBrowser->append(str);
        str.sprintf(" Считано байт %d\n",BytesReceived);ui->FTDI_List_textBrowser->append(str);
        str.sprintf(" Данные : %s\n",RxBuffer);ui->FTDI_List_textBrowser->append(str);
*/            }
             else {
                    // FT_Read Failed
/*                     str.sprintf("Не удалось считать данные, RxBytes       = %s\n", udata); ui->FTDI_List_textBrowser->append(str);
                     numBytes = BytesReceived; itoa(numBytes, udata,10);
                     str.sprintf("                         BytesReceived = %s\n", udata); ui->FTDI_List_textBrowser->append(str);
*/                  //   ui->FTDI_List_textBrowser->append("Не удалось считать данные);

             }


}



//==================================================
/*
FT_HANDLE MainWindow::ftdi_set_mode(int DevNumber_ff)

{
    ftStatus = FT_GetDeviceInfoDetail(DevNumber_ff, &Flags, &Type, &ID, &LocId, SerialNumber, Description, &ftHandle);
    ftStatus = FT_Open(DevNumber_ff, &ftHandle);
    if(ftStatus != FT_OK)
    {
        str.sprintf("Open Failed with error %d\n", ftStatus);
        FT_Close(ftHandle);
    }
    Mode = 0x00; //reset mode
    ftStatus |= FT_SetBitMode(ftHandle, Mask, Mode); // Mask
    //mleep(10);
    //Sleep(1000); //Желательно сделать паузу
    Mode = 0x40; //Sync FIFO mode
    ftStatus |= FT_SetBitMode(ftHandle, Mask, Mode);

    if (ftStatus != FT_OK)
    {
       str.sprintf("Error in initializing1 %d\n", ftStatus);ui->FTDI_List_textBrowser->append(str);
        FT_Close(ftHandle);
        //return 1; // Exit with error
    }

    ftStatus |= FT_SetLatencyTimer(ftHandle, LatencyTimer);
    ftStatus |= FT_SetUSBParameters(ftHandle,0x10000,0x10000);
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);//0x10,0x13
    if (ftStatus == FT_OK)
    {
    str.sprintf(" Включен режим Synchronous FIFO");ui->FTDI_List_textBrowser->append(str);
    //access data from here
    str.sprintf(" FTHANDLE %d : 0x%x\n", DevNumber_ff, ftHandle);ui->FTDI_List_textBrowser->append(str);

    }
    else
    {
    // FT_SetBitMode FAILED!
     str.sprintf(" Не могу включить режим Synchronous FIFO");ui->FTDI_List_textBrowser->append(str);
     FT_Close(ftHandle);
    // return 1;
    }

  //  ftStatus = FT_Purge(ftHandle, FT_PURGE_RX);
    ftStatus = FT_Purge(ftHandle, FT_PURGE_TX | FT_PURGE_RX);

    if (ftStatus == FT_OK)
    {
    str.sprintf(" Буферы очищены");ui->FTDI_List_textBrowser->append(str);

    }
    else
    {
     str.sprintf(" Не могу очистить буферы");ui->FTDI_List_textBrowser->append(str);
     FT_Close(ftHandle);

    }

// Set read timeout of 1sec, write timeout of 0.5sec
    ftStatus = FT_SetTimeouts(ftHandle, 1000, 500);
    if (ftStatus == FT_OK)  { ;// FT_SetTimeouts OK
    }
    else
        { // FT_SetTimeouts failed
           str.sprintf(" Не могу задать таймауты");ui->FTDI_List_textBrowser->append(str);
           FT_Close(ftHandle);
        }


     ftStatus |= FT_GetQueueStatus(ftHandle, &dwNumBytesToRead);
     if ((ftStatus == FT_OK) && (dwNumBytesToRead > 0))
     {

         str.sprintf(" Есть данные в очереди");ui->FTDI_List_textBrowser->append(str);


     }
     else str.sprintf(" Нет данных в очереди");ui->FTDI_List_textBrowser->append(str);

     return ftHandle;
}
*/
//===================================================

