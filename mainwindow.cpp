/*
Вся суть окна пользователя - дать ему возможность сформировать команду КАМАК.
Далее эта команда отправляется в USB стандартными средствами.

*/


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QByteArray>
#include <stdlib.h>
#include <stdio.h>
#include <QDataStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
   // ui->FTDI_List_textBrowser->locale();

   // ui->Data2Send_lineEdit->setText("AABBCCDD0102CE3F");
//++++++++++++++++++++++++++++++
    ui->N_spinBox->setAlignment(Qt::AlignCenter); ui->N_spinBox->setFocus();
    ui->A_spinBox->setAlignment(Qt::AlignCenter);
    ui->F_spinBox->setAlignment(Qt::AlignCenter);
    //Начальные значения
    N_CAMAC = 2; //
    A_CAMAC = 0;
    F_CAMAC = 0;

    NAF_Bits = 0x0000;
    ToDev_Bits = 0;

    X = FALSE;
    Q = FALSE;

    A_Open = FALSE;
    B_Open = FALSE;
//+++++++++++++++++++++++++++++++++++++
    ftdi_init();

    timer = new QTimer(this);
    connect(timer, SIGNAL( timeout() ),this , SLOT( onTimer() ) );
    timer->start(500); //Проверяем наличие подключения. Запускаем таймер пока потоки не запустили.
    // ============================================== Создаем поток 1 - Канал А
        thread_A = new QThread;
        chan_A = new MyThread();


        chan_A->moveToThread(thread_A);

        connect(thread_A, SIGNAL(started()), chan_A, SLOT(A_SLOT()) );
        connect(chan_A, SIGNAL(A_finished()), thread_A, SLOT(quit()));
        connect(chan_A, SIGNAL(Process_A()), this, SLOT(Read_A())); //Считываем из железки в ПК по каналу А

        chan_A->finthread = FALSE;
        // ============================================== Создаем поток 2 - Канал B
            thread_B = new QThread;
            chan_B = new MyThread();


            chan_B->moveToThread(thread_B);

            connect(thread_B, SIGNAL(started()), chan_B, SLOT(B_SLOT()) );
            connect(chan_B, SIGNAL(B_finished()), thread_B, SLOT(quit()));
            connect(chan_B, SIGNAL(Process_B()), this, SLOT(Write_B()));

            chan_B->finthread = FALSE;

}

MainWindow::~MainWindow()
{
    delete ui;
}
//+++++++++++++++++++++++++++++++++++
void MainWindow::paintEvent(QPaintEvent *e)
{
   Q_UNUSED(e);
   QPainter ekPaint(this);

//    QRect rec(180, 780,30,30);

   framepen.setWidth(20);
   if (X) framepen.setColor(Qt::green);
   else framepen.setColor(Qt::black); //Меняем цвет кружка red

   ekPaint.setPen(framepen);
   ekPaint.drawEllipse(40, 520, 20,20 ); //Левй кружок, коробка №1
 //  ekPaint.drawEllipse(180, 780, 20,20 );

   if (Q) framepen.setColor(Qt::green); //DEV_2_ON
   else framepen.setColor(Qt::red); //Меняем цвет кружка

   ekPaint.setPen(framepen);
   ekPaint.drawEllipse(120, 520, 20,20 );

   //http://www.evileg.ru/baza-znanij/qt/qpainter-znakomstvo-s-risovaniem-v-qt.html
   //https://www.youtube.com/watch?v=gqFxNBBi4Q0

   int i, x;
   UINT16 rdata;
   //UINT8 byte;

  // rdata = 0x66aa;
   rdata = byInputBuffer[3]; rdata<<=8;
   rdata += byInputBuffer[4];

   x = 0; //y = 460;

   for (i=15; i>=0; i--)//рисуем индикаторы слева направо, поэтому номера идут со старшего.
   {
      if ((rdata>>i) & 0x01) {framepen.setColor(Qt::red);}
      else framepen.setColor(Qt::black);
      ekPaint.setPen(framepen);
      if  (!((i+1)%4)) {x+=20;}
      x = x + 49;
      ekPaint.drawEllipse(x, 460, 19,19 );

   } //for

   //++++ индикатор связи
   if (FTDI_Connected) framepen.setColor(Qt::green); //
   else framepen.setColor(Qt::red); //Тревога

   ekPaint.setPen(framepen);

   ekPaint.drawEllipse(940, 28, 19,19 );
}
//+++++++++++++++++++++++++++++++++++
//Очистка буферов, необходима чтобы очищались индикаторы данных.
void MainWindow::Purge_IO_Buffers(void)
{
memset(byOutputBuffer, 0, sizeof(byOutputBuffer));
memset(byInputBuffer, 0, sizeof(byInputBuffer));


}

//void MainWindow::on_SEND_TO_USB_pushButton_clicked() {//str = "00       00       00         00";}

void MainWindow::on_N_spinBox_valueChanged(int arg1)
{
    if (arg1 > N_MAXVALUE)
    {
        N_CAMAC = N_MAXVALUE;
        ui->N_spinBox->setValue(N_MAXVALUE);
        return;

    }
    if (arg1 < N_MINVALUE)
    {
        N_CAMAC = N_MINVALUE;
        ui->N_spinBox->setValue(N_MINVALUE);
        return;
    }

    else N_CAMAC = arg1;

    Update_Label();

    //++++++++++ Тут меняем подпись в строке состояние команды


}
//+++++++++++++++++++++++++++++++++++++++

void MainWindow::Update_Label(void)
{

    CAMAC_make_command(); //Заполняем массив на передачу данных в USB

    QString str;

 //    strcpy(byOutputBuffer, str.constData());
    QByteArray mystr = QByteArray(reinterpret_cast<char*>(byOutputBuffer), 16);
    mystr.resize(4);
    str = mystr.toHex().toUpper();
    str.insert(2,"       "); //+7
    str.insert(11,"       "); //+7
    str.insert(20,"         "); //+7
   // str.insert(26,"      "); //+8

    ui->NAF_Data_Label->clear();
    ui->NAF_Data_Label->setText(str);


}
//+++++++++++ Ниже ф-ция лишняя, нужно убрать
void MainWindow::on_N_spinBox_editingFinished()
{
    if (ui->N_spinBox->value() > N_MAXVALUE)
    {
        ui->N_spinBox->setFocus();

    }
}
//+++++++++++++++++
void MainWindow::on_A_spinBox_valueChanged(int arg1)
{
    if (arg1 > A_MAXVALUE)
    {
        A_CAMAC = A_MAXVALUE;
        ui->A_spinBox->setValue(A_MAXVALUE);
        return;
    }

    if (arg1 < A_MINVALUE)
    {
        A_CAMAC = A_MINVALUE;
        ui->A_spinBox->setValue(A_MINVALUE);
        return;
    }


    else A_CAMAC = arg1;
    Update_Label();

}

void MainWindow::on_A_spinBox_editingFinished()
{
    if (A_CAMAC > A_MAXVALUE)
    {
        ui->A_spinBox->setFocus();

    }

}

void MainWindow::on_F_spinBox_valueChanged(int arg1)
{
    if (arg1 > F_MAXVALUE)
    {
        F_CAMAC = F_MAXVALUE;
        ui->F_spinBox->setValue(F_MAXVALUE);
        return;
    }
    if (arg1 < F_MINVALUE)
    {
        F_CAMAC = F_MINVALUE;
        ui->F_spinBox->setValue(F_MINVALUE);
        return;
    }


  else F_CAMAC = arg1;
    Update_Label();

}

void MainWindow::on_F_spinBox_editingFinished()
{
    if (F_CAMAC > F_MAXVALUE)
    {
        ui->F_spinBox->setFocus();

    }

}
//+++++++++++++++++++++++++++=
void MainWindow::CAMAC_make_command(void)
{
    //N_CAMAC = 2..23
    //F_CAMAC = 0..31
    //A_CAMAC = 0..15
    UINT16 naf;
 //   char byOutputBuffer[64];
//++++++++++++++++++++++++++++ биты 7,8 не определены +++++++++++++++

    naf = N_CAMAC; //naf <<= 8; //naf & 0x1fff;
    NAF_Bits = naf & 0x001f; // N16,N8,N4,N2,N1 на 5-1 битах

    naf = F_CAMAC;
    NAF_Bits |=  ((naf<<1) & 0x0020);  //F16 на 6-м бите
    NAF_Bits |=  ((naf<<12) & 0xf000); //F8,F4,F2,F1 на 16-13 битах

    naf = A_CAMAC;
    NAF_Bits |=  ( (naf << 8) & 0x0f00); // A8,A4,A2,A1 на 12-09 битах

// Теперь NAF_Bits и ToDev_Bits объединяем в 1 32 битное слово
    CAMAC_WORD = ToDev_Bits; CAMAC_WORD <<=16;
    CAMAC_WORD += NAF_Bits;

    byOutputBuffer[0]= (CAMAC_WORD & 0x000000ff);
    byOutputBuffer[1]= (CAMAC_WORD & 0x0000ff00)>>8;
    byOutputBuffer[2]= (CAMAC_WORD & 0x00ff0000)>>16;
    byOutputBuffer[3]= (CAMAC_WORD & 0xff000000)>>24;


}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Clear_All_clicked()
{
    //Начальные значения
    N_CAMAC = 2; //
    A_CAMAC = 0;
    F_CAMAC = 0;

    NAF_Bits = 0x0000;
    ToDev_Bits = 0;
    QString str = "00       00       00         00";

    ui->N_spinBox->setValue(N_CAMAC);
    ui->A_spinBox->setValue(A_CAMAC);
    ui->F_spinBox->setValue(F_CAMAC);

    ui->NAF_Data_Label->setText(str);

    if (ui->Sel_Bit_01->isChecked()) ui->Sel_Bit_01->toggle();
    if (ui->Sel_Bit_02->isChecked()) ui->Sel_Bit_02->toggle();
    if (ui->Sel_Bit_03->isChecked()) ui->Sel_Bit_03->toggle();
    if (ui->Sel_Bit_04->isChecked()) ui->Sel_Bit_04->toggle();
    if (ui->Sel_Bit_05->isChecked()) ui->Sel_Bit_05->toggle();
    if (ui->Sel_Bit_06->isChecked()) ui->Sel_Bit_06->toggle();
    if (ui->Sel_Bit_07->isChecked()) ui->Sel_Bit_07->toggle();
    if (ui->Sel_Bit_08->isChecked()) ui->Sel_Bit_08->toggle();
    if (ui->Sel_Bit_09->isChecked()) ui->Sel_Bit_09->toggle();
    if (ui->Sel_Bit_10->isChecked()) ui->Sel_Bit_10->toggle();
    if (ui->Sel_Bit_11->isChecked()) ui->Sel_Bit_11->toggle();
    if (ui->Sel_Bit_12->isChecked()) ui->Sel_Bit_12->toggle();
    if (ui->Sel_Bit_13->isChecked()) ui->Sel_Bit_13->toggle();
    if (ui->Sel_Bit_14->isChecked()) ui->Sel_Bit_14->toggle();
    if (ui->Sel_Bit_15->isChecked()) ui->Sel_Bit_15->toggle();
    if (ui->Sel_Bit_16->isChecked()) ui->Sel_Bit_16->toggle();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_16_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_16->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x8000; //16 bit set to '1'
    }
    else
    {  ui->Sel_Bit_16->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0x7fff; //16 bit set to '0'
    }
  Update_Label();
}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_15_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_15->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x4000; //15 bit set to '1'
    }
    else
    {  ui->Sel_Bit_15->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xBfff; //15 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_14_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_14->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x2000; //14 bit set to '1'
    }
    else
    {  ui->Sel_Bit_14->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xDfff; //14 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_13_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_13->setStyleSheet("background-color: yellow");
       ToDev_Bits |=0x1000; //13 bit set to '1'
    }
    else
    {  ui->Sel_Bit_13->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xEfff; //13 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_12_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_12->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0800; //12 bit set to '1'
    }
    else
    {  ui->Sel_Bit_12->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xf7ff; //12 bit set to '0'
    }
    Update_Label();
}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_11_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_11->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0400; //11 bit set to '1'
    }
    else
    {  ui->Sel_Bit_11->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfBff; //11 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_10_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_10->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0200; //10 bit set to '1'
    }
    else
    {  ui->Sel_Bit_10->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfDff; //10 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_09_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_09->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0100; //09 bit set to '1'
    }
    else
    {  ui->Sel_Bit_09->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfEff; //09 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_08_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_08->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0080; //08 bit set to '1'
    }
    else
    {  ui->Sel_Bit_08->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xff7f; //08 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_07_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_07->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0040; //07 bit set to '1'
    }
    else
    {  ui->Sel_Bit_07->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xffBf; //07 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_06_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_06->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0020; //06 bit set to '1'
    }
    else
    {  ui->Sel_Bit_06->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xffDf; //06 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_05_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_05->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0010; //05 bit set to '1'
    }
    else
    {  ui->Sel_Bit_05->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xffEf; //05 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_04_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_04->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0008; //04 bit set to '1'
    }
    else
    {  ui->Sel_Bit_04->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfff7; //04 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_03_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_03->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0004; //03 bit set to '1'
    }
    else
    {  ui->Sel_Bit_03->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfffB; //03 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_02_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_02->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0002; //02 bit set to '1'
    }
    else
    {  ui->Sel_Bit_02->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfffD; //02 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++
void MainWindow::on_Sel_Bit_01_toggled(bool checked)
{
    if(checked) {
       ui->Sel_Bit_01->setStyleSheet("background-color: yellow");
       ToDev_Bits |= 0x0001; //01 bit set to '1'
    }
    else
    {  ui->Sel_Bit_01->setStyleSheet("background-color: lightgray");
       ToDev_Bits &= 0xfffE; //01 bit set to '0'
    }
  Update_Label();

}
//+++++++++++++++++++++++++++++++++++





/*
void MainWindow::on_Get_Stop_pushButton_clicked()

{

}


void MainWindow::on_GET_Stop_pushButton_clicked()
{

}


void MainWindow::on_pushButton_stop_Sending_clicked()
{

}
*/
