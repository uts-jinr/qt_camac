#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QObject>
#include <QThread>

class MyThread : public QObject
{
    Q_OBJECT
public:
  //  explicit MyThread(QObject *parent = 0);
     MyThread();
    ~MyThread();
    bool finthread;   //Флаг работы самого потока. Поток закрываем по нажатию кнопок в окне пользователя
/*    bool run_thread;  // Флаг работы цикла внутри потока
    long  usb_counter;
*/

public slots:
        void A_SLOT();  /* Канал А, Прием данных в ПК из железа  */
        void B_SLOT();  /* Канал В, отправка данных из ПК в железо */
//        void Start_Polling();




signals:
//        void onTime_Elapsed();    /* Сигнал счетчика - "заданное время отсчитано" */
      //  void Polling_finished();    /* Сигнал о завершении цикла поллинга в другом потоке*/
        void Process_A();
        void Process_B();
        void A_finished();            /* Сигнал о завершении работы Потока... одного или всех ? или того, в котором он сигнал вызывается ? */
        void B_finished();
      //  void B_finished();
  //      void Five_Cycles_Passed();

  //      void error(QString err);  /* Сигнал об ошибке в работе счетчика */




};




#endif // MYTHREAD_H
