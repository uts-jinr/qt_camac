#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qt_windows.h>
#include <windows.h>
#include "ftd2xx.h"
#include "mythread.h"
#include <QTimer>
#include <QFile>
#include <QtGui>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QTimer      *timer;
    QFile        mfile;
    QString     filename;

    void ftdi_init(void);
    FT_HANDLE ftdi_set_mode(int DevNumber_ff);
    void on_pushButton_Read_data_clicked();

    BOOL FTDI_Connected; //Флаг наличия связи с контроллером
    BOOL A_Open, B_Open; //Флаги открытия порта А, В


    //+++++++++++++++++++++++++++++ Threads +++++++++++++++
    // FT2232H has 2 channels A & B, So we create 2 Threads for A & B channel eachone
    //MyThread *timing_1, *polling_2;
    MyThread *chan_A, *chan_B;
    QThread *thread_A, *thread_B;
//++++++++++++++++++++++++++++++++++++++++
    void CAMAC_make_command(void);  //На основе данных в окне пользователя формируем команду камак на отправку в USB
    void Update_Label(void);  //Обновляем значение 4 байт команды при изменении эл-тов управляения
    void Data_Parser(void);   //Анализ пришедших данных
    UINT8 N_CAMAC, A_CAMAC, F_CAMAC;
#define N_MAXVALUE 23
#define A_MAXVALUE 15
#define F_MAXVALUE 31
#define N_MINVALUE  2 //[0-1] - занимает контроллер крейта.
#define A_MINVALUE  0
#define F_MINVALUE  0

    UINT16 ToDev_Bits; // Биты данных в команде для КАМАК
    UINT16 NAF_Bits;   // Биты  NAF в команде для КАМАК
    UINT32 CAMAC_WORD; // Команда КАМАК состоит из 4 байт.

    QPen framepen; //Лампочки сигналов X, Q
    BOOL X, Q;
//++++++++++++++++++++++++++++++++ USB STAFF ++++++++++++
#define TRANSFER_SIZE 4
#define RECEIVE_SIZE  TRANSFER_SIZE + 4

    char byOutputBuffer[TRANSFER_SIZE]; // Массив данных на отправку в USB
    char byInputBuffer [64]; // Buffer to hold data readed from the FT2232H  RECEIVE_SIZE

    void Purge_IO_Buffers(void);


public slots:
//    void on_SEND_pushButton_clicked();
    void on_GET_Stop_pushButton_clicked();
    void on_pushButton_stop_Sending_clicked();


    void Write_B(); // Пишем в USB chan B
    void Read_A();  //Читаем из  USB chan A
    void onTimer();  //Обработчик таймера

private slots:





    void on_GET_pushButton_clicked();



//    void on_GET_Stop_pushButton_clicked();
//++++++++++++++++++++++++++++++++++++++++++++++
    void on_SEND_TO_USB_pushButton_clicked();

    void on_N_spinBox_valueChanged(int arg1);

    void on_N_spinBox_editingFinished();

    void on_A_spinBox_valueChanged(int arg1);

    void on_A_spinBox_editingFinished();

    void on_F_spinBox_valueChanged(int arg1);

    void on_F_spinBox_editingFinished();

    void on_Clear_All_clicked();

    void on_Sel_Bit_16_toggled(bool checked);

    void on_Sel_Bit_15_toggled(bool checked);

    void on_Sel_Bit_14_toggled(bool checked);

    void on_Sel_Bit_13_toggled(bool checked);

    void on_Sel_Bit_12_toggled(bool checked);

    void on_Sel_Bit_11_toggled(bool checked);

    void on_Sel_Bit_10_toggled(bool checked);

    void on_Sel_Bit_09_toggled(bool checked);

    void on_Sel_Bit_08_toggled(bool checked);

    void on_Sel_Bit_07_toggled(bool checked);

    void on_Sel_Bit_06_toggled(bool checked);

    void on_Sel_Bit_05_toggled(bool checked);

    void on_Sel_Bit_04_toggled(bool checked);

    void on_Sel_Bit_03_toggled(bool checked);

    void on_Sel_Bit_02_toggled(bool checked);

    void on_Sel_Bit_01_toggled(bool checked);







private:
    Ui::MainWindow *ui;
protected:
  virtual void paintEvent(QPaintEvent *e);

};

#endif // MAINWINDOW_H
